#!/bin/bash

echo "Checking Internet connection...";

if ping -c3 8.8.8.8;
	then echo "Internet access OK";
		exit 0;
	else echo "Not connected to Internet";
	     echo "Please check configuration";
		exit;
fi
